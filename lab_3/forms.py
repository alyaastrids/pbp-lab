from django.forms import ModelForm, DateInput
from lab_1.models import Friend

class DOBInput(DateInput):
    input_type = 'date'

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets = {'dob' : DOBInput()}