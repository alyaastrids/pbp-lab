import 'package:flutter/material.dart';


class CardItem2 extends StatefulWidget {
  const CardItem2 ({Key? key}) : super(key: key);

  @override
  _CardItem2State createState() => _CardItem2State();
}

class _CardItem2State extends State<CardItem2> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child:
                  Column(
                      children: [
                        SizedBox(height: 20,),
                        Text('Cara Pencegahan Covid-19',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        Text("Untuk mencegah penyebaran COVID-19: - Selalu jaga jarak aman dari orang lain (minimal 1 meter), meskipun mereka tidak tampak sakit. Kenakan masker di ruang publik, terutama di dalam ruangan atau jika pembatasan fisik tidak dimungkinkan. - Sebaiknya pilih ruang terbuka dan berventilasi baik. Buka jendela jika berada di dalam ruangan. - Cuci tangan Anda secara rutin. Gunakan sabun dan air, atau cairan pembersih tangan berbahan alkohol. - Ikuti vaksinasi ketika giliran Anda. Ikuti panduan setempat terkait vaksinasi.", style: TextStyle(
                          fontSize: 15,
                        ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        // Row(
                        //   children: [
                            Text("Sumber : Kemenkes", style: TextStyle(
                              fontSize: 12,
                            ),
                              textAlign: TextAlign.center,),
                        //   ],
                        // ),
                        // SizedBox(height: 20,),
                        // ElevatedButton(
                        //   style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),
                        //     primary: Colors.green,
                        //     onPrimary: Colors.white,
                        //     side: BorderSide(width: 2, color: Colors.green),
                        //     padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
                        //     shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),),
                        //   onPressed: () {},
                        //   child: const Text('View Details'),
                        // ),
                      ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
