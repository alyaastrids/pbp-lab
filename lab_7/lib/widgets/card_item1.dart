import 'package:flutter/material.dart';


class CardItem1 extends StatefulWidget {
  const CardItem1 ({Key? key}) : super(key: key);

  @override
  _CardItem1State createState() => _CardItem1State();
}

class _CardItem1State extends State<CardItem1> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child:
                  Column(
                      children: [
                        SizedBox(height: 20,),
                        Text('Apa itu Covid-19?',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        Text("Infeksi virus Corona disebut COVID-19 (Corona Virus Disease 2019) dan pertama kali ditemukan di kota Wuhan, China pada akhir Desember 2019. Virus ini menular dengan sangat cepat dan telah menyebar ke hampir semua negara, termasuk Indonesia, hanya dalam waktu beberapa bulan. Hal tersebut membuat beberapa negara menerapkan kebijakan untuk memberlakukan lockdown dalam rangka mencegah penyebaran virus Corona. Di Indonesia sendiri, pemerintah menerapkan kebijakan Pemberlakukan Pembatasan Kegiatan Masyarakat (PPKM) untuk menekan penyebaran virus ini. ", style: TextStyle(
                          fontSize: 15,
                        ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        // Row(
                        //   children: [
                            Text("Sumber : Alodokter", style: TextStyle(
                              fontSize: 12,
                            ),
                              textAlign: TextAlign.center,),
                          // ],
                        // ),
                        // SizedBox(height: 20,),
                        // ElevatedButton(
                        //   style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),
                        //     primary: Colors.green,
                        //     onPrimary: Colors.white,
                        //     side: BorderSide(width: 2, color: Colors.green),
                        //     padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
                        //     shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),),
                        //   onPressed: () {},
                        //   child: const Text('View Details'),
                        // ),
                      ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
