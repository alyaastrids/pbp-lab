import 'package:flutter/material.dart';


class CardItem3 extends StatefulWidget {
  const CardItem3 ({Key? key}) : super(key: key);

  @override
  _CardItem3State createState() => _CardItem3State();
}

class _CardItem3State extends State<CardItem3> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child:
                  Column(
                      children: [
                        SizedBox(height: 20,),
                        Text('Vaksinasi Covid-19 kepada Anak',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        Text("Juru Bicara Satgas Penanganan COVID-19 Prof. Wiku Adisasmito menjelaskan pemberian vaksin COVID-19 pada anak usia 6 - 11 tahun akan dilakukan setelah cakupan vaksinasi nasional tercapai. Sebagaimana yang disampaikan Menteri Koordinator Bidang Pembangunan Manusia dan Kebudayaan (PMK) bahwa vaksin anak akan diberikan setelah cakupan dosis pertama nasional melebihi 70% dari total sasaran target vaksinasi dan lebih dari 60% populasi lansia. Wiku berkata bahwa vaksinasi ini dimulai dari kabupaten/kota yang telah memenuhi target tersebut. Kemudian pemerintah berusaha mencapai target ini di akhir tahun 2021.", style: TextStyle(
                          fontSize: 15,
                        ),
                          textAlign: TextAlign.center,),
                        SizedBox(height: 20,),
                        // Row(
                        //   children: [
                            Text("Sumber : covid19.go.id", style: TextStyle(
                              fontSize: 12,
                            ),
                              textAlign: TextAlign.center,),
                        //   ],
                        // ),
                        // SizedBox(height: 20,),
                        // ElevatedButton(
                        //   style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),
                        //     primary: Colors.green,
                        //     onPrimary: Colors.white,
                        //     side: BorderSide(width: 2, color: Colors.green),
                        //     padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
                        //     shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),),
                        //   onPressed: () {},
                        //   child: const Text('View Details'),
                        // ),
                      ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
