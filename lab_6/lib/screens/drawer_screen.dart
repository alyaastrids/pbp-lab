import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Alya Astrid"),
            accountEmail: Text("alyaastrids29@gmail.com"),
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.map,
            title: "Peta Sebaran",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.attach_money,
            title: "Donasi",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.location_city,
            title: "Destinasi Tutup",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.my_library_books,
            title: "Edukasi",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.chat,
            title: "Forum",
            onTilePressed: () {
            },
          ),
          DrawerListTile(
            iconData: Icons.shopping_bag,
            title: "Supplies",
            onTilePressed: () {
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData ;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile({Key? key, required this.iconData, required this.title, required this.onTilePressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title, style: TextStyle(fontSize: 16),),
    );
  }
}
