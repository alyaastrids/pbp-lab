import 'package:flutter/material.dart';
// import 'package:carousel_slider/carousel_slider.dart';
import '../widgets/card_item1.dart';
import '../widgets/card_item2.dart';
import '../widgets/card_item3.dart';

class Home extends StatefulWidget {
  // static const routeName = '/home-page';
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget> [
        SizedBox(height: 50),
        CardItem1(),
        CardItem2(),
        CardItem3(),
        SizedBox(height: 20),
        ElevatedButton(
          style: ElevatedButton.styleFrom(textStyle: TextStyle(fontSize: 20),
            primary: Colors.green,
            onPrimary: Colors.white,
            side: BorderSide(width: 2, color: Colors.green),
            padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),),
          onPressed: () {},
          child: Text('Selengkapnya'),
        ),
        SizedBox(height: 50),
      ],
    );
  }
}

