from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_2.models import Note

# Create your views here.
def index(request):
    catatan = Note.objects.all()
    response = {'catatan': catatan}
    return render(request, 'lab2.html', response)

def xml(request):
    catatan = Note.objects.all() 
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    catatan = Note.objects.all()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json") 