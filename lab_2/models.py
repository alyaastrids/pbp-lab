from django.db import models

# Create your models here.
class Note(models.Model):
    untuk = models.CharField(max_length=15)
    dari = models.CharField(max_length=15)
    judul = models.CharField(max_length=15)
    pesan = models.TextField()