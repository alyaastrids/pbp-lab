from django.urls import path
from lab_4.views import index, add_note, note_list

urlpatterns = [
    path('', index, name = "indeks"),
    path('add', add_note, name = "add_note"),
    path('list', note_list, name = "note_list")
]